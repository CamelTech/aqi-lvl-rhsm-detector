#!/usr/bin/env python

import os
import json
import datetime
import time
import schedule
from pprint import pprint

# clears console for readability
os.system('clear')

def getData():
	# generate timestamp
	now = datetime.datetime.now()
	timeStamp = "SE:" + str(now.second) + ",MI:" + str(now.minute) + ",HO:" + str(now.hour) + ",DA:" + str(now.day) + ",MO:" + str(now.month) + ",YE:" + str(now.year)
	print("Getting data at: " + timeStamp + "...")

	# delete the tmp JSONDATA.json file
	print("removing JSONData.json...")
	os.system('rm JSONData.json')
	# make request for pollution data
	print("Making request...")
	os.system('curl -H "Content-Type: application/json" -X GET "https://api.aerisapi.com/airquality/84102?&format=json&client_id=TbFoGYK8jaGNl11Dq2Gnw&client_secret=m2v5U09oHzV3LXXMSag4QWnnp9Wwvuf6k3zqCH0P" > JSONData.json')

	# load data from file
	print("loading data from file...")
	with open('JSONData.json', 'r') as f:
	    data = json.load(f)

	# parse data
	print("Parsing data...")
	data = data["response"][0]["periods"][0]["pollutants"]

	pm25AQI = data[1].get("aqi")
	pm25Cat = data[1].get("category")

	pm10AQI = data[2].get("aqi")
	pm10Cat = data[2].get("category")

	carbonMonoxideAQI = data[3].get("aqi")
	carbonMonoxideCat = data[3].get("category")

	nitrogenDioxideAQI = data[4].get("aqi")
	nitrogenDioxideCat = data[4].get("category")

	# writes data
	print("Writting data to quality.txt...")
	with open("../quality.txt", "a+") as f:
		f.write("{}\nPM2.5:\n\tAQI:{}\n\tCAT: {}\nPM10:\n\tAQI: {}\n\tCAT: {}\nCARBON-MONOXIDE:\n\tAQI: {}\n\tCAT: {}\nNITROGEN-DIOXIDE:\n\tAQI: {}\n\tCAT: {}\n\n\n".format(timeStamp, pm25AQI, pm25Cat, pm10AQI, pm10Cat, carbonMonoxideAQI, carbonMonoxideCat, nitrogenDioxideAQI, nitrogenDioxideCat))
	
	print("Waiting...")

# what times to run (24 hour time)
print("Setting run schedule...")
schedule.every().monday.at("12:00").do(getData)
schedule.every().tuesday.at("12:00").do(getData)
schedule.every().wednesday.at("12:00").do(getData)
schedule.every().thursday.at("12:00").do(getData)
schedule.every().friday.at("12:00").do(getData)
schedule.every().saturday.at("12:00").do(getData)
schedule.every().sunday.friday.at("12:00").do(getData)

print("Waiting...")

while True:
	# if it is time to run, run, but if not, wait 1 sec and repeat
	getData()
    time.sleep(86400)
	